#pragma once        /* json.h - A small JSON parser based in C, with    */
#include <stdio.h>  /*          many limitations. It can parse, add and */
#include <stdlib.h> /*          dump JSON strings.                      */
#include <string.h> /* Written by B[], 2022                             */
typedef struct json{ char *k, *v; struct json* c; int n; } json;
void json_add(json* j, json* c){ j->c = (struct json*)realloc(j->c,
  ++(j->n) * sizeof(json)); memcpy(&(j->c[j->n - 1]) , c, sizeof(json)); free(c); }
char* cc(char** a, char* b){ int l = strlen(*a); int n = strlen(b) + 1;
  *a = realloc(*a, l + n); memcpy(*a + l, b, n); return *a; }
char* cs(char* s){ char* z = malloc(1); *z = 0; return cc(&z, s); }
json* json_parse(char* s){ json* j = (json*)malloc(sizeof(json)); memset(j, 0, sizeof(json));
  char* e = s + strlen(s) - 1; while(*(s += strcspn(s, "\"{}[]"))){ char c = *s;
  *s = 0; ++s; if(c == '"'){ s = strtok(s, "\""); if(!j->k) j->k = cs(s);
  else j->v = cs(s); }else if(c == '{' || c == '[') json_add(j, json_parse(s));
  else return j; for(s = e; *s; --s); ++s; } return j; }
json* json_open(char* n){ FILE* f = fopen(n, "r"); fseek(f, 0, SEEK_END);
  int l = ftell(f); char* s = malloc(l + 1); rewind(f); fread(s, l, 1, f);
  fclose(f); s[l] = 0; json* j = json_parse(s); free(s); return j; }
char* json_dump(json* j){ char* s = malloc(1); *s = 0; if(j->k){ cc(&s, "\"");
  cc(&s, j->k); cc(&s, "\""); } if(j->n){ if(j->k) cc(&s, ":");
  for(int x = 0; x < j->n; x++){ if(x) cc(&s, ","); s = cc(&s, "{"); char* c;
  c = json_dump(&(j->c[x])); cc(&s, c); free(c); cc(&s, "}"); } }else if(j->v){
  cc(&s, ":\""); cc(&s, j->v); cc(&s, "\""); } return s; } char* JSON = "0.8.0";
void json_free(json* j){ for(int x = 0; x < j->n; x++) json_free(&(j->c[x]));
  if(j->k) free(j->k); if(j->v) free(j->v); if(j->c) free(j->c); j->n = 0; }
