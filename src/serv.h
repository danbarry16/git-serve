#pragma once            /* serv.h - A lightweight multi-threaded server */
#include <netinet/in.h> /*          library written in C. The server    */
#include <stdlib.h>     /*          parses useful HTTP information such */
#include <string.h>     /*          as method, location, headers. The   */
#include <sys/wait.h>   /*          lib then writes header and data.    */
#include <unistd.h>     /* Written by B[], 2022                         */
#define out(c, s) {write(c, s, strlen(s)); write(c, "\r\n", 2);}
typedef struct{ int f, l; struct sockaddr_in a, c; } serv; int L = 65536;
unsigned int H(char* s){ int h; for(h = 0; *s; s++) h = ((h << 5) + h) + *s; return h; }
void serv_init(serv* s, int p){ s->f = socket(AF_INET, SOCK_STREAM, 0);
  s->a.sin_family = AF_INET; s->a.sin_addr.s_addr = INADDR_ANY;
  s->a.sin_port = htons(p); bind(s->f, (struct sockaddr*)&(s->a), sizeof(s->a));
  listen(s->f, 5); s->l = sizeof(s->c); }
void serv_read(int c, char** h){ char b[L], *k; read(c, b, L); char* t = strtok(b, " ");
  h[H("request") % L] = t; t = strtok(NULL, " "); k = "location"; while(k != NULL){
  while(*k < 32) ++k; h[H(k) % L] = t; k = strtok(NULL, " ="); t = strtok(NULL, "\n&"); } }
void serv_write(int c, char** h){ out(c, h[H("response") % L]); int t = 0;
  while(++t < 9) if(h[t]) out(c, h[t]); out(c, ""); out(c, h[H("payload") % L]); }
void serv_update(serv* s, void (*p)(char**, char**)){ waitpid(-1, NULL, WNOHANG);
  int c = accept(s->f, (struct sockaddr*)&(s->c), &(s->l)); if(c < 0 || fork() != 0) return;
  char** r = (char**)malloc(L * sizeof(char*)); memset(r, 0, L * sizeof(char*));
  char** q = (char**)malloc(L * sizeof(char*)); memset(q, 0, L * sizeof(char*));
  serv_read(c, r); (*p)(r, q); serv_write(c, q); shutdown(c, SHUT_RDWR);
  close(c); free(q); free(r); } char* SERV = "0.5.0";
