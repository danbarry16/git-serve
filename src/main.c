#include "git.h"
#include "json.h"
#include "serv.h"

#include <sys/time.h>

typedef struct{ char* url; git g; long long to; char* name; char* desc; } repo;

json* j;
char* url;
int repos_n;
repo* repos;

/**
 * json_get()
 *
 * Get a JSON object by a given key.
 *
 * @param j The JSON object to be searched.
 * @param k The key string.
 * @return The JSON object, otherwise NULL.
 **/
json* json_get(json* j, char* k){
  json* o = NULL;
  for(int x = 0; x < j->n; x++){
    if(strcmp(j->c[x].k, k) == 0){
      o = &(j->c[x]);
      break;
    }
  }
  return o;
}

/**
 * current_time_millis()
 *
 * Get the current time in milliseconds.
 *
 * @return The current time in milliseconds.
 **/
long long current_time_millis(){
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((long long)t.tv_sec) * 1000 + ((long long)t.tv_usec) / 1000;
}

/**
 * sanitize_commit()
 *
 * Ensure commit letters are within range, otherwise NULL the bad characters
 * (causing the string to terminate early).
 *
 * @param c The commit string to be checked.
 * @return The processed string.
 **/
char* sanitize_commit(char* c){
  for(int x = 0; c != NULL && c[x] != '\0'; x++)
    if(!((c[x] >= '0' && c[x] <= '9') ||
         (c[x] >= 'a' && c[x] <= 'z') ||
         (c[x] >= 'A' && c[x] <= 'Z') )) c[x] = '\0';
  return c;
}

/**
 * sanitize_html()
 *
 * Sanitize data for displaying in HTML pages. This is done by replacing
 * problematic ASCII characters with others.
 *
 * @param s The string to be sanitized.
 * @param n The maximum length of the string to be produced.
 * @return The processed string.
 **/
char* sanitize_html(char* s, int n){
  char* z = (char*)malloc(n * sizeof(char));
  int i = 0;
  for(int x = 0; s != NULL && s[x] != '\0'; x++){
    /* TODO: This is unlikely to work with unicode. */
    if(i + 1 + 6 >= n) break; // Space for \0 and largest escape sequence
    switch(s[x]){
      case '&' :
        z[i++] = '&';
        z[i++] = 'a';
        z[i++] = 'm';
        z[i++] = 'p';
        z[i++] = ';';
        break;
      case ' ' :
        /* Check if the previous character was a space */
        if(x > 0 && s[x - 1] == ' '){
          z[i++] = '&';
          z[i++] = 'n';
          z[i++] = 'b';
          z[i++] = 's';
          z[i++] = 'p';
          z[i++] = ';';
        }else{
          z[i++] = s[x];
        }
        break;
      case '<' :
        z[i++] = '&';
        z[i++] = 'l';
        z[i++] = 't';
        z[i++] = ';';
        break;
      case '>' :
        z[i++] = '&';
        z[i++] = 'g';
        z[i++] = 't';
        z[i++] = ';';
        break;
      default :
        z[i++] = s[x];
        break;
    }
  }
  z[i++] = '\0';
  return z;
}

/**
 * sanitize_shell()
 *
 * Sanitize the shell string to prevent escaping.
 *
 * @param s The string to be escaped.
 * @return The processed string.
 **/
char* sanitize_shell(char* s){
  for(int x = 0; s != NULL && s[x] != '\0'; x++){
    /* TODO: This should really be checked. */
    /* TODO: This is unlikely to work with unicode. */
    if(s[x] == '.' && s[x + 1] == '.') s[x] = '\0';
    if(s[x] == ';') s[x] = '\0';
    if(s[x] == '&') s[x] = '\0';
    if(s[x] == '|') s[x] = '\0';
    if(s[x] == '$') s[x] = '\0';
    if(s[x] == '<') s[x] = '\0';
    if(s[x] == '>') s[x] = '\0';
  }
  return s;
}

/**
 * update_repo()
 *
 * If a client thread is really unlucky, they may be have the fetch and pull
 * happen whilst they are connecting!
 *
 * @param r The repo to be checked.
 **/
void update_repo(int r){
  if(r < 0 || r >= repos_n) return;
  if(repos[r].to > current_time_millis()) return;
  json* repo = &(json_get(j, "repo")->c[0]);
  repos[r].to = current_time_millis() + atol(json_get(repo, "timeout-ms")->v);
  char** res;
  res = g_fetch(&(repos[r].g));
  g_free(res);
  res = g_pull(&(repos[r].g));
  g_free(res);
}

/**
 * process()
 *
 * Process a given client on a dedicated thread.
 *
 * @param r The request headers.
 * @param q The return headers.
 **/
void process(char** r, char** q){
  long long t_beg = current_time_millis();
  json* repo = &(json_get(j, "repo")->c[0]);
  char* req = r[H("request") % L];
  char* loc = r[H("location") % L];
  q[H("response") % L] = "HTTP/1.1 200 OK";
  char* body = (char*)malloc(L * sizeof(char));
  if(strcmp(req, "GET") == 0){
    /* Index */
    if(strcmp(loc, "/"     ) == 0 ||
       strstr(loc, "/index") == loc){
      sprintf(body, "<h2>Welcome</h2>%s", json_get(j, "msg")->v);
    /* List repositories */
    }else if(strstr(loc, "/o") == loc){
      sprintf(body, "<h2>Repos</h2><ul>");
      for(int x = 0; x < repos_n; x++){
        char* san_name = sanitize_html(repos[x].name, L);
        char* san_desc = sanitize_html(repos[x].desc, L);
        sprintf(&body[strlen(body)],
          "<li><a href=\"%s/r/%i\">%s</a> - <i>%s</i></li>",
          url, x, san_name, san_desc);
        free(san_name);
        free(san_desc);
      }
      sprintf(&body[strlen(body)], "</ul>");
    /* Repository index */
    }else if(strstr(loc, "/r/") == loc){
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      if(r < 0 || r >= repos_n) sprintf(body, "<h2>Unknown repository</h2>");
      else{
        sprintf(body, json_get(repo, "nav")->v, url, r, url, r, url, r, url, r);
        char* san_name = sanitize_html(repos[r].name, L);
        char* san_desc = sanitize_html(repos[r].desc, L);
        sprintf(&body[strlen(body)],
          "<h2>%s</h2>"
          "<h3>Description</h3>"
          "%s",
          san_name,
          san_desc
        );
        free(san_name);
        free(san_desc);
      }
    /* Repository logs */
    }else if(strstr(loc, "/l/") == loc){
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      if(l != NULL) l = strtok(NULL, "/");
      int s = l != NULL ? atoi(l) : 0;
      if     (r < 0 || r >= repos_n) sprintf(body, "<h2>Unknown repository</h2>");
      else if(s < 0                ) sprintf(body, "<h2>Invalid log skip</h2>");
      else{
        sprintf(body, json_get(repo, "nav")->v, url, r, url, r, url, r, url, r);
        char* san_name = sanitize_html(repos[r].name, L);
        sprintf(&body[strlen(body)],
          "<h2>%s</h2>"
          "<h3>Logs</h3>"
          "<ul>",
          san_name
        );
        free(san_name);
        json* jgit = &(json_get(j, "git")->c[0]);
        int n = atoi(json_get(jgit, "log-count")->v);
        char** res = g_logn(&(repos[r].g), s, n);
        for(int x = 0; x < n; x++){
          char* commit = sanitize_commit(strtok(res[x], " "));
          char* desc = strtok(NULL, "\n");
          if(commit != NULL && desc != NULL){
            char* san_desc = sanitize_html(desc, L);
            sprintf(&body[strlen(body)],
              "<li><a href=\"%s/c/%i/%s\">%s</a> - %s</li>",
              url,
              r,
              commit,
              commit,
              san_desc
            );
            free(san_desc);
          }
        }
        g_free(res);
        sprintf(&body[strlen(body)], "</ul>");
        if(s >= n)
          sprintf(&body[strlen(body)], "<a href=\"%s/l/%i/%i\">Prev</a> | ", url, r, s - n);
        sprintf(&body[strlen(body)],   "<a href=\"%s/l/%i/%i\">Next</a>",    url, r, s + n);
      }
    /* Repository commit */
    }else if(strstr(loc, "/c/") == loc){
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      if(l != NULL) l = strtok(NULL, "/");
      char* c = l != NULL ? sanitize_commit(l) : NULL;
      if     (r < 0 || r >= repos_n) sprintf(body, "<h2>Unknown repository</h2>");
      else if(c == NULL            ) sprintf(body, "<h2>Invalid commit</h2>");
      else{
        sprintf(body, json_get(repo, "nav")->v, url, r, url, r, url, r, url, r);
        char* san_name = sanitize_html(repos[r].name, L);
        sprintf(&body[strlen(body)],
          "<h2>%s</h2>"
          "<h3>Commit %s</h3>",
          san_name,
          c
        );
        free(san_name);
        char** res = g_logc(&(repos[r].g), c);
        for(int x = 0; x < N && res[x][0] != '\0'; x++)
          sprintf(&body[strlen(body)], "<br>%s", res[x]);
        g_free(res);
      }
    /* Repository tree */
    }else if(strstr(loc, "/t/") == loc){
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      char* d = strtok(NULL, "\n");
      if(r < 0 || r >= repos_n) sprintf(body, "<h2>Unknown repository</h2>");
      else{
        sprintf(body, json_get(repo, "nav")->v, url, r, url, r, url, r, url, r);
        char* san_name = sanitize_html(repos[r].name, L);
        sprintf(&body[strlen(body)],
          "<h2>%s</h2>"
          "<h3>Tree</h3>"
          "<ul>",
          san_name
        );
        free(san_name);
        if(d != NULL){
          char* path = (char*)malloc(N * sizeof(char));
          sprintf(path, "%s/%s", repos[r].g.d, d);
          repos[r].g.d = sanitize_shell(path);
        }
        char** res = g_tree(&(repos[r].g));
        for(int x = 0; x < N && res[x][0] != '\0'; x++){
          char* t = strtok(res[x], " ");
          t = t != NULL ? strtok(NULL, " ") : NULL;
          char* f = t != NULL ? strtok(NULL, "\t") : NULL;
          f = f != NULL ? strtok(NULL, "\n") : NULL;
          char* san_f = sanitize_html(f, L);
          if(t != NULL && f != NULL){
            if(strcmp(t, "tree") == 0)
              sprintf(&body[strlen(body)],
                "<li>d | <a href=\"%s/t/%i/%s\">%s/</a></li>",
                url,
                r,
                san_f,
                san_f
              );
            else if(strcmp(t, "blob") == 0)
              sprintf(&body[strlen(body)],
                "<li>f | <a href=\"%s/f/%i/0/%s\">%s</a></li>",
                url,
                r,
                san_f,
                san_f
              );
            else sprintf(&body[strlen(body)], "<li>? | %s</li>", san_f);
          }
          free(san_f);
        }
        if(repos[r].g.d != NULL) free(repos[r].g.d);
        g_free(res);
        sprintf(&body[strlen(body)], "</ul>");
      }
    /* Repository file */
    }else if(strstr(loc, "/f/") == loc){
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      if(l != NULL) l = strtok(NULL, "/");
      int s = l != NULL ? atoi(l) : 0;
      char* d = strtok(NULL, "\n");
      if     (r < 0 || r >= repos_n) sprintf(body, "<h2>Unknown repository</h2>");
      else if(s < 0                ) sprintf(body, "<h2>Invalid log skip</h2>");
      else{
        sprintf(body, json_get(repo, "nav")->v, url, r, url, r, url, r, url, r);
        char* san_name = sanitize_html(repos[r].name, L);
        sprintf(&body[strlen(body)],
          "<h2>%s</h2>"
          "<h3>File</h3>",
          san_name
        );
        free(san_name);
        /* TODO: White-list extensions? */
        char* f = d;
        if(d != NULL){
          char* t = strrchr(d, '/');
          if(t != NULL){
            *t = '\0';
            f = t + 1;
          }else{
            d = "";
          }
          char* path = (char*)malloc(N * sizeof(char));
          sprintf(path, "%s/%s", repos[r].g.d, d);
          repos[r].g.d = sanitize_shell(path);
        }
        json* jgit = &(json_get(j, "git")->c[0]);
        int n = atoi(json_get(jgit, "line-count")->v);
        /* Location in file decided by URL */
        char** res = g_blame(&(repos[r].g), f, s + 1, n);
        for(int x = 0; x < N && res[x][0] != '\0'; x++){
          char* san_res = sanitize_html(res[x], L);
          char* commit = strtok(san_res, " )");
          char* line = strtok(NULL, " )");
          sprintf(&body[strlen(body)],
            "<a href=\"%s/c/%i/%s\">%s</a>%s<br>",
            url,
            r,
            commit,
            line,
            line + strlen(line) + 1
          );
          free(san_res);
        }
        if(repos[r].g.d != NULL) free(repos[r].g.d);
        g_free(res);
        if(s >= n)
          sprintf(&body[strlen(body)], "<a href=\"%s/f/%i/%i/%s/%s\">Prev</a> | ", url, r, s - n, d, f);
        sprintf(&body[strlen(body)],   "<a href=\"%s/f/%i/%i/%s/%s\">Next</a>",    url, r, s + n, d, f);
      }
    /* Repository RSS */
    }else if(strstr(loc, "/x/") == loc){
      /* Clean-up normal HTML response, this is XML instead */
      free(body);
      char* buff = (char*)malloc(L * sizeof(char));
      /* Start processing the request */
      char* l = strtok(&loc[3], "/");
      int r = atoi(l);
      if(r < 0 || r >= repos_n) sprintf(buff, "<h2>Unknown repository</h2>");
      else{
        int port = atoi(json_get(j, "port")->v);
        char* san_name = sanitize_html(repos[r].name, L);
        char* san_desc = sanitize_html(repos[r].desc, L);
        sprintf(buff,
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<rss version=\"2.0\">"
            "<channel>"
              "<title>%s</title>"
              "<description>%s</description>"
              "<link>%s</link>",
          san_name,
          san_desc,
          url
        );
        free(san_name);
        free(san_desc);
        json* jgit = &(json_get(j, "git")->c[0]);
        int n = atoi(json_get(jgit, "log-count")->v);
        char** res = g_logn(&(repos[r].g), 0, n);
        for(int x = 0; x < n; x++){
          char* commit = sanitize_commit(strtok(res[x], " "));
          char* desc = strtok(NULL, "\n");
          char* san_desc = sanitize_html(desc, L);
          if(commit != NULL && desc != NULL){
            sprintf(&buff[strlen(buff)],
              "<item>"
                "<title>%s</title>"
                "<description>%s</description>"
                "<link>%s/c/%i/%s</link>"
              "</item>",
              commit,
              san_desc,
              url,
              r,
              commit
            );
          }
          free(san_desc);
        }
        g_free(res);
        sprintf(&buff[strlen(buff)], "</channel></rss>");
        /* Return payload (early) */
        q[H("payload") % L] = buff;
        return;
      }
    }else sprintf(body, "<h2>Unknown location</h2>");
  }else sprintf(body, "<h2>Unknown request</h2>");
  /* Dress-up response */
  char* buff = (char*)malloc(L * sizeof(char));
  long long t_end = current_time_millis();
  sprintf(buff,
    "<html>"
      "<head>"
        "<title>%s</title>"
        "<style>%s</style>"
      "</head>"
      "<body>"
        "<h1>%s</h1>",
    json_get(j, "name")->v,
    json_get(j, "css")->v,
    json_get(j, "name")->v
  );
  sprintf(&buff[strlen(buff)], json_get(j, "nav")->v, url, url);
  sprintf(&buff[strlen(buff)],
        "<hr>%s<hr>"
        "%s"
        "<br>Took %lli ms."
      "</body>"
    "</html>",
    body,
    json_get(j, "foot")->v,
    t_end - t_beg
  );
  free(body);
  /* Return payload */
  q[H("payload") % L] = buff;
}

/**
 * main()
 *
 * The main entry point into the program. Here we parse the command line
 * arguments and start the program as required.
 *
 * @param argc The number of command line arguments.
 * @param argv The command line arguments.
 **/
int main(int argc, char** argv){
  /* Setup default variables */
  char* cfg = "cfg/def.json";
  /* Process command line arguments */
  for(int x = 0; x < argc; x++){
    if(argv[x][0] == '-'){
      switch(argv[x][1]){
        case 'c' :
          if(++x < argc) cfg = argv[x];
          break;
        case 'h' :
          printf("git_serv [OPT]\n");
          printf("  OPTions\n");
          printf("    -c  Set configuration file\n");
          printf("    -h  Display this help\n");
          return 0;
      }
    }
  }
  /* Load configuration file */
  j = &(json_open(cfg)->c[0]);
  url = json_get(j, "url")->v;
  int port = atoi(json_get(j, "port")->v);
  /* Populate list of repos */
  json* r = &(json_get(j, "repos")->c[0]);
  repos_n = r->n;
  repos = (repo*)malloc(repos_n * sizeof(repo));
  for(int x = 0; x < repos_n; x++){
    json* c = &(r->c[x]);
    repos[x].url = json_get(c, "url")->v;
    repos[x].g.d = json_get(c, "dir")->v;
    repos[x].g.r = json_get(c, "remote")->v;
    repos[x].g.b = json_get(c, "branch")->v;
    repos[x].to = current_time_millis();
    repos[x].name = json_get(c, "name")->v;
    repos[x].desc = json_get(c, "desc")->v;
    /* Perform update on start */
    update_repo(x);
  }
  /* Initialise the server */
  printf("Starting server on port %i\n", port);
  serv s = {};
  serv_init(&s, port);
  /* Pick-up some clients */
  unsigned int x = 0;
  int pid = getpid();
  while(pid == getpid()){
    serv_update(&s, process);
    /* For each client, take it in turns to check a repo on main thread */
    if(repos_n > 0) update_repo(x++ % repos_n);
  }
  /* Return nicely */
  return 0;
}
