FLAGS := -std=c99 -g -D_XOPEN_SOURCE

.PHONY : clean test all

all : clean build

build: clean
	cc $(FLAGS) -o git_serv src/main.c

test : clean build
	./git_serv

clean :
	-rm git_serv
